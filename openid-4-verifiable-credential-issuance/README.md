# openid-connect-4-credential-issuance
Specification to allow holders to request issuance of credentials and issuers to issue verifiable credentials.

### Migrated to GitHub ###

Migrated on 1st Septemer 2023

https://github.com/openid/OpenID4VCI
