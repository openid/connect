# OpenID Federation
Defines how parties within a federation can establish trust with one another

### Migrated to GitHub ###

The repository for OpenID Federation moved to https://github.com/openid/federation following the publication of [Implementer's Draft 4](https://openid.net/specs/openid-federation-1_0-ID4.html) in July 2024.
